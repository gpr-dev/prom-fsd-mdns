# prom-fsd-mdns

Tool that dynamically creates a Prometheus file-based service discovery configuration file (json) from mDNS query.

This tool has been inspired by [prometheus-mdns-sd](https://github.com/msiebuhr/prometheus-mdns-sd).

## Quick Start

To fetch, build and install the `prom-fsd-mdns` command:

```
go get gitlab.com/gpr-dev/prom-fsd-mdns/...
```

### Docker image

`prom-fsd-mdns` is also available through Docker:

```
docker run --rm registry.gitlab.com/gpr-dev/prom-fsd-mdns
docker run --rm registry.gitlab.com/gpr-dev/prom-fsd-mdns:<git tag>
docker run --rm registry.gitlab.com/gpr-dev/prom-fsd-mdns:master
```

Available tags:
- _master_: image built from the _master_ branch
- _\<git tag\>_: image built from official tag

### Static binaries

TBD

## References

- [Prometheus file-based service discovery](https://prometheus.io/docs/prometheus/latest/configuration/configuration/#file_sd_config)