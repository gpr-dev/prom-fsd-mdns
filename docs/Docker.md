# Building the Docker image

The project includes a [Dockerfile](/build/package/Dockerfile) that allows to
generate a lightweight image.

```
docker build -t prom-fsd-mdns -f build/package/Dockerfile .
```
